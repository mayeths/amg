if [[ $# -ne 1 ]]; then
    echo "$0 <timestamp>"
    exit 1
fi

LOGDIR=amglog.$1

mkdir -p $LOGDIR
mv amglog.*-1.$1 amglog.*-2.$1 amglog.*-3.$1 amglog.*-4.$1 $LOGDIR
tar -cf $LOGDIR.tar $LOGDIR
realpath $LOGDIR.tar
