#!/bin/bash

if uname -m | grep "x86" >/dev/null; then
        echo "You are using x86 machine. (Huang Haopeng)"
        exit 1
fi

echo "For better result, please exit this machine."
sleep 10

MAX=4
ARR_PX=(2)
ARR_PY=(12 11 10 9 8 7 6 5 4 3 2 1)
ARR_PZ=(4)

NX=100
NY=100
NZ=100
SUFFIX=$(date +%y%m%d-%H%M%S)
OPT="--cpu-set 0-95"
OPT="$OPT --bind-to cpulist:ordered --report-bindings"

doit() {
    if [[ $# -ne 4 ]]; then
        echo "Wrong argument number $#"
        return 1
    fi
    local PX=$1
    local PY=$2
    local PZ=$3
    local ID=$4

    echo "mpirun $OPT -np $((PX * PY * PZ)) amg -n $NX $NY $NZ -P $PX $PY $PZ –printstats"
    mpirun $OPT -np $((PX * PY * PZ)) amg -n $NX $NY $NZ -P $PX $PY $PZ –printstats > stdout 2> stderr

    local LOG="amglog.$((PX * PY * PZ)).$ID.$PX-$PY-$PZ.$SUFFIX"
    mkdir -p $LOG
    mv stdout stderr outfile.rank* *start* *stop* $LOG
}

for i in $(seq 1 $MAX); do

    for PX in ${ARR_PX[@]}; do
        for PY in ${ARR_PY[@]}; do
            for PZ in ${ARR_PZ[@]}; do
                echo ">>> ($i/$MAX) Working on $PX $PY $PZ"
                doit $PX $PY $PZ $i
            done
        done
    done

done

